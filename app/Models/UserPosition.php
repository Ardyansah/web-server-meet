<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $meeting_id
 * @property float $lat
 * @property float $lng
 * @property float $speed
 * @property float $heading
 * @property string $location_name
 * @property string $flag
 * @property string $meeting_due
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property Meeting $meeting
 */
class UserPosition extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'meeting_id', 'lat', 'lng', 'speed', 'heading', 'location_name', 'flag', 'meeting_due', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function meeting()
    {
        return $this->belongsTo('App\Models\Meeting');
    }
}
