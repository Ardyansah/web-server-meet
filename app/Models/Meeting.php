<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 
 * @property integer $id
 * @property integer $initiator
 * @property string $meet_eta
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property MeetingChat[] $meetingChats
 * @property MeetingMember[] $meetingMembers
 */
class Meeting extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['initiator', 'meet_eta', 'status', 'lat', 'lng', 'finished_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'initiator');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function meetingChats()
    {
        return $this->hasMany('App\Models\MeetingChat');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function meetingMembers()
    {
        return $this->hasMany('App\Models\MeetingMember');
    }
}
