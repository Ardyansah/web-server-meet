<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $tour_group_id
 * @property integer $user_id
 * @property integer $admin
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property TourGroup $tourGroup
 */
class TourGroupMember extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['tour_group_id', 'user_id', 'admin', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tourGroup()
    {
        return $this->belongsTo('App\Models\TourGroup');
    }
}
