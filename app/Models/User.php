<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $google_auth_code
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $phone
 * @property string $photo_url
 * @property string $token
 * @property integer $is_guide
 * @property string $created_at
 * @property string $updated_at
 * @property MeetingChat[] $meetingChats
 * @property MeetingMember[] $meetingMembers
 * @property Meeting[] $meetings
 * @property TourDestination[] $tourDestinations
 * @property TourGroupChat[] $tourGroupChats
 * @property TourGroupMember[] $tourGroupMembers
 * @property TourGroup[] $tourGroups
 * @property UserContact[] $userContacts
 * @property UserContact[] $userContacts
 * @property UserPosition[] $userPositions
 */
class User extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['google_auth_code', 'firstname', 'lastname', 'email', 'phone', 'photo_url', 'token', 'is_guide', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function meetingChats()
    {
        return $this->hasMany('App\Models\MeetingChat');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function meetingMembers()
    {
        return $this->hasMany('App\Models\MeetingMember');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function meetings()
    {
        return $this->hasMany('App\Models\Meeting', 'initiator');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tourDestinations()
    {
        return $this->hasMany('App\Models\TourDestination', 'creator');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tourGroupChats()
    {
        return $this->hasMany('App\Models\TourGroupChat');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tourGroupMembers()
    {
        return $this->hasMany('App\Models\TourGroupMember');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tourGroups()
    {
        return $this->hasMany('App\Models\TourGroup', 'initiator');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userContacts()
    {
        return $this->hasMany('App\Models\UserContact');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userInContacts()
    {
        return $this->hasMany('App\Models\UserContact', 'contact_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userPositions()
    {
        return $this->hasMany('App\Models\UserPosition');
    }
}
