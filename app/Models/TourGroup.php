<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $initiator
 * @property int $active_waypoint
 * @property string $group_name
 * @property string $description
 * @property integer $active
 * @property string $created_at
 * @property string $deleted_at
 * @property TourDestination $tourDestination
 * @property User $user
 * @property TourGroupChat[] $tourGroupChats
 * @property TourGroupMember[] $tourGroupMembers
 */
class TourGroup extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['initiator', 'active_waypoint', 'group_name', 'description', 'active', 'created_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tourDestination()
    {
        return $this->belongsTo('App\Models\TourDestination', 'active_waypoint');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function initiated_by()
    {
        return $this->belongsTo('App\Models\User', 'initiator');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tourGroupChats()
    {
        return $this->hasMany('App\Models\TourGroupChat', 'group_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tourGroupMembers()
    {
        return $this->hasMany('App\Models\TourGroupMember');
    }
}
