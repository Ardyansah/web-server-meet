<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChatController extends Controller
{
    function send(Request $r){
        $chat = \App\Models\MeetingChat::create($r->all());

        return response()->json($chat,201);
    }

    function retreive($meetid, $lastid = -1){
        $meeting = \App\Models\Meeting::find($meetid);
        if($meeting === null){
            return response()->json(null,404);
        }

        $chats = $meeting->meetingChat()->where("id",">",$lastid)->get();

        return response()->json($chats);
    }
}
