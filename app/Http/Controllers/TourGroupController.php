<?php

namespace App\Http\Controllers;

use App\Models\TourDestination;
use App\Models\TourGroup;
use App\Models\TourGroupChat;
use App\Models\User;
use Illuminate\Http\Request;

class TourGroupController extends Controller
{
    function getByInitiator(Request $r, User $initiator)
    {
        return response()->json($initiator->tourGroups()->with(['initiated_by'])->get());
    }
    function createByInitiator(Request $r, User $initiator)
    {
        $createGroup = null;
        if ($r->input('existingId')) {
            $createGroup = TourGroup::find($r->input('existingId'));
            $createGroup->tourGroupMembers()->delete();
        } else {
            $createGroup = $initiator->tourGroups()->create($r->all());
        }
        $createGroup->tourGroupMembers()->create([
            "user_id" => $initiator->id,
            "admin" => 1
        ]);
        foreach ($r->input('members') as $userid) {
            if ($userid == $initiator->id) continue;
            $createGroup->tourGroupMembers()->create([
                "user_id" => $userid,
                "admin" => 0
            ]);
        }
        $createGroup->load(
            'tourDestination',
            'initiated_by',
            'tourGroupMembers',
            'tourGroupMembers.user'
        );
        return response()->json($createGroup);
    }
    function getMemberByGroup(Request $r, TourGroup $group)
    {
        return response()->json($group->tourGroupMembers()->get());
    }
    function getChats(Request $r, TourGroup $group)
    {
        return response()->json($group->tourGroupChats()->with(['user'])->get());
    }
    function getMemberLocations(Request $r, TourGroup $group)
    {
    }

    function deleteGroup(TourGroup $group)
    {
        $group->tourGroupChats()->delete();
        $group->tourGroupMembers()->delete();
        $group->delete();
        return response()->json([]);
    }

    function getByUser(Request $r, User $user)
    {
        $groupdata = $user->tourGroupMembers()->with(['tourGroup', 'tourGroup.initiated_by'])->get();
        $groups = $groupdata->map(function ($gd) {
            $gd->tourGroup->load('tourGroupMembers');
            $gd->tourGroup->load('tourDestination');
            return $gd->tourGroup;
        });

        return response()->json($groups);
    }

    function sendChat(Request $r)
    {
        $chat = TourGroupChat::create($r->all());
        $chat->user;

        return response()->json($chat);
    }

    function getGroupInfo(TourGroup $group)
    {
        $group->load(
            'tourDestination',
            'initiated_by',
            'tourGroupMembers',
            'tourGroupMembers.user'
        );
        return response()->json($group);
    }
    function setTourDestination(TourGroup $group, TourDestination $destination)
    {
        $group->active_waypoint = $destination->id;

        $group->save();

        $group->load(
            'tourDestination',
            'initiated_by',
            'tourGroupMembers',
            'tourGroupMembers.user'
        );

        return response()->json($group);
    }

    function clearTourDestination(TourGroup $group)
    {
        $group->active_waypoint = null;
        $group->tourDestination;
        $group->initiated_by;
        $group->tourGroupMembers;

        $group->save();
        return response()->json($group);
    }
}
