<?php

namespace App\Http\Controllers;

use App\Mail\GuideRegistrator;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PeopleController extends Controller
{
    function profile($id)
    {
        $profile = \App\Models\User::find($id);
        return response()->json($profile);
    }
    function contacts($id)
    {
        $user = \App\Models\User::find($id);
        if ($user === null) {
            return response()->json(null, 404);
        }

        $contacts = $user->userContacts()->with(['contact'])->get();

        $c = json_decode(json_encode($contacts));

        foreach ($c as &$k) {
            $k->contact->user_positions = [\App\Models\UserPosition::where("user_id", $k->contact->id)->latest()->first()];
        }

        return $c;
    }

    function addContact($id, $contact_id)
    {
        $user = \App\Models\User::find($id);
        if ($user === null) {
            return response()->json(null, 404);
        }

        $check = $user->userContacts()->where("contact_id", $contact_id)->get();
        if ($check->count() > 0) {
            return response()->json(null, 202);
        }

        $user->userContacts()->create([
            "contact_id" => $contact_id
        ]);

        return response()->json(null, 201);
    }
    function userlist(Request $r)
    {
        $search = $r->input("s");
        $data = \App\Models\User::where("email", 'like', "%$search%")
            ->orWhere("firstname", 'like', "%$search%")
            ->orWhere("lastname", 'like', "%$search%")
            ->get();
        return response()->json($data);
    }
    function setAsGuide(Request $r, User $id)
    {
        $email = [];
        $email[] = Mail::to("rnugraha305@gmail.com")->sendNow(new GuideRegistrator($id));
        $email[] = Mail::to("hamkah900@gmail.com")->sendNow(new GuideRegistrator($id));
        $email[] = Mail::to("hamka.ardyansah@yahoo.com")->sendNow(new GuideRegistrator($id));
        $email[] = Mail::to("hamka.ardyansyah@anabatic.com")->sendNow(new GuideRegistrator($id));
        // $user = $id;
        // $user->is_guide = 1;
        // $user->save();
        $id->is_guide = 2;
        $id->save();

        $id->sendmail = $email;
        return response()->json($id);
    }
    function acceptGuide(User $user)
    {
        $user->is_guide = 1;
        $user->save();
        return response()->json(["Success, $user->firstname is now a Tour Guide"]);
    }
}
