<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MeetingController extends Controller
{
    function getList($id){
        $user = \App\Models\User::find($id);
        if($user === null){
            return response()->json(null,404);
        }

        $meetings = $user
        ->meetings()
        ->latest()
        ->with(["meetingMembers","meetingMembers.user","user"])
        ->get();

        $meetings = $meetings->toArray();

        $asMember = \App\Models\MeetingMember::where("user_id",$user->id)->with(["meeting","meeting.meetingMembers","meeting.user","meeting.meetingMembers.user"])->get();

        foreach($asMember as $member){
            array_push($meetings,$member->meeting->toArray());
        }
        // dd($meetings);
        usort($meetings,function($a,$b){
            return $a['created_at'] < $b['created_at'];
        });

        return response()->json($meetings);
    }
    function create(Request $r){
        $meeting = \App\Models\Meeting::create($r->input("meeting"));
        foreach($r->input("members") as $memberid){
            $meeting->meetingMembers()->create([
                "user_id"=>$memberid
            ]);
        }

        $meeting->meetingMembers;
        $meet = \App\Models\Meeting::with(["meetingMembers","meetingMembers.user"])->find($meeting->id);

        return response()->json($meet,201);
    }
    function getLocations(Request $r){
        $ids = $r->input("ids");
        $data = \App\Models\UserPosition::whereIn("user_id",$ids)->get();

        return response()->json($data);
    }
    function get($id){
        $meeting = \App\Models\Meeting::with(["meetingMembers","meetingMembers.user"])->find($id);
        return response()->json($meeting);
    }    
    function getTracks($meetingid, $userid){
        $meeting = \App\Models\Meeting::find($meetingid);
        if($meeting === null){
            return response()->json(null,404);
        }

        $data = \App\Models\UserPosition::where("user_id",$userid)
            ->where("created_at",">=",$meeting->created_at)
            ->orderBy("created_at","desc")
            ->get();

        return response()->json($data);
        
    }
    function finish($id){
        $meeting = \App\Models\Meeting::find($id);
        if($meeting === null){
            return response()->json(null,404);
        }
        $meeting->status = "finished";
        $meeting->finished_at = date("Y-m-d H:i:s");
        $meeting->save();

        foreach($meeting->meetingMembers as $member){
            // var_dump($member->toArray());
            $m = \App\Models\UserPosition::where("user_id",$member->user->id)->where("created_at",">=",$meeting->created_at)->latest()->first();
            if($m !== null){
                $m->flag = "finish";
                $m->meeting_id = $meeting->id;
                $m->meeting_due = $meeting->meet_eta;

                $m->save();
            }
        }

        return response()->json(null,202);
    }
}
